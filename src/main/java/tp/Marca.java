package tp;

/**
 * Created by user on 31/05/17.
 */
public class Marca {
    private static int id_loca = 0;
    private int id;
    private String descripcion;

    public Marca(String n){
        id_loca++;
        id = id_loca;
        descripcion = n;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
}
