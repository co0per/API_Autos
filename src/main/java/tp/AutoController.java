package tp;

import java.util.ArrayList;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

//@Controller
@RestController
public class AutoController {

    ArrayList<Auto> autos = new ArrayList<Auto>();

    public AutoController()
    {
        Marca ford = new Marca("Ford");
        Marca vol = new Marca("Volkswagen");

        Auto uno = new Auto(ford, "Focus", 50000, "HOL 444", 2003);
        Auto dos = new Auto(vol, "Golf", 482930, "SOR 373", 1999);
        Auto tres = new Auto(ford, "Mustang", 150432, "MAL 000", 1990);
        autos.add(uno);
        autos.add(dos);
        autos.add(tres);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Auto> getAllCars() {
        return this.autos;
    }

    @RequestMapping(value = "id", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Auto getCarByID(@RequestParam(value="ID", required=true) int ID)
    {
        for(int i = 0; i < this.autos.size(); i++){
            if (this.autos.get(i).getId() == ID){
                return this.autos.get(i);
            }
        }
        return null;
    }

    @RequestMapping(value = "byMarca", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Auto> getCarByBrand(@RequestParam(value="MARCA", required=true) String MARCA)
    {
        ArrayList<Auto> autitos = new ArrayList<Auto>();

        for(int i = 0; i < this.autos.size(); i++){
            if (this.autos.get(i).getMarca().getDescripcion().equals(MARCA)){
                autitos.add(this.autos.get(i));
            }
        }
        return autitos;
    }

    @RequestMapping(value = "/insertarAuto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void insertCar(@RequestBody Auto car) {
        autos.add(car);
    }
}