package tp;

public class Auto {
    private static int id_loca = 0;
    private int id;
    private Marca marca;
    private String modelo;
    private double kilometros;
    private String patente;
    private int anio;

    public Auto(Marca mrc, String mdl, double km, String p, int a){
        id_loca++;
        id = id_loca;
        marca = mrc;
        modelo = mdl;
        kilometros = km;
        patente = p;
        anio = a;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public Marca getMarca() { return marca; }
    public void setMarca(Marca marca) { this.marca = marca; }
    public String getModelo() { return modelo; }
    public void setModelo(String modelo) { this.modelo = modelo; }
    public double getKilometros() { return kilometros; }
    public void setKilometros(double kilometros) { this.kilometros = kilometros; }
    public int getAnio() { return anio; }
    public void setAnio(int anio) { this.anio = anio; }
    public String getPatente() { return patente; }
    public void setPatente(String patente) { this.patente = patente; }
}